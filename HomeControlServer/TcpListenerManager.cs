﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace HomeControlServer
{
    public class TcpListenerManager
    {
        IPAddress _ip;
        int _port;

        IPAddress Ip
        {
            get
            {
                return _ip;
            }

            set
            {
                if(value != null && value != _ip)
                {
                    _ip = value;
                }
            }
        }

        int Port
        {
            get
            {
                return _port;
            }

            set
            {
                if(value != _port)
                {
                    _port = value;
                }
            }
        }

        public List<TcpClient> ConnectedClients { get; set; } = new List<TcpClient>();

        public TcpListenerManager(IPAddress ip, int port)
        {
            _ip = ip;
            _port = port;
        }


    }
}
