﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HomeControlServer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        TcpListener listener;

        Thread listenerThread, LoopClientThread;

        List<TcpClient> connectedClients = new List<TcpClient>();

        bool keepListening = true;

        public MainWindow()
        {
            InitializeComponent();
            listener = new TcpListener(IPAddress.Parse("192.168.2.102"), 8448);
            listenerThread = new Thread(new ThreadStart(ReadListenerData));
            LoopClientThread = new Thread(new ThreadStart(LoopClients));

            try
            {
                listener.Start();
                Debug.WriteLine("Listener started");
            }
            catch(Exception ex)
            {
                MessageBox.Show($"Could not open listener on specified port and ip address... Details:{Environment.NewLine}{ex.ToString()}", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            listenerThread.Start();
            LoopClientThread.Start();
        }

        private void ReadListenerData()
        {
            while(keepListening)
            {
                try
                {
                    TcpClient newClient = listener.AcceptTcpClient();
                    if (!connectedClients.Contains(newClient))
                    {
                        connectedClients.Add(newClient);
                        Debug.WriteLine($"New client connected: {newClient.ToString()}");
                    }
                    
                }
                catch(Exception ex)
                {
                    Debug.WriteLine(ex.ToString());
                }
            }
        }

        private void LoopClients()
        {
            while (keepListening)
            {
                Debug.WriteLine("while in client loop");
                try
                {
                    NetworkStream dataStream;
                    StreamReader reader;
                    StreamWriter writer;
                    string DataString = "";
                    byte[] dataBuffer = new byte[4096];

                    foreach (TcpClient client in connectedClients)
                    {
                        Debug.WriteLine("Foreach");
                        dataStream = client.GetStream();

                        //int i;
                        //while ((i = dataStream.Read(dataBuffer, 0, dataBuffer.Length)) != 0)
                        //{
                        //    DataString = Encoding.ASCII.GetString(dataBuffer, 0, 0);
                        //}
                        reader = new StreamReader(dataStream);



                        Debug.WriteLine("This happens before the reading...");
                        DataString = reader.ReadLine();
                        Debug.WriteLine("This happens after the reading...");

                        Debug.WriteLine(DataString);
                        reader.Close();

                        writer = new StreamWriter(dataStream);

                        writer.WriteLine(DataString);

                        writer.Flush();

                        writer.Close();

                        Debug.WriteLine(DataString);

                        //dataStream.Write(dataBuffer, 0, dataBuffer.Length);
                    }
                }
                catch(Exception ex)
                {
                    Debug.WriteLine(ex.ToString());
                }
            }
        }
    }
}
